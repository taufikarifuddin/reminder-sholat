package com.app.monitoringapps.callbacks;

import android.location.Location;
import android.location.LocationListener;

/**
 * TODO: Add a class header comment!
 */

public interface UserLocationCallback {
    public void getData(Location location, LocationListener listener);
}
