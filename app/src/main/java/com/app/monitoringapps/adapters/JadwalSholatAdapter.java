package com.app.monitoringapps.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.monitoringapps.dto.JadwalSholatDTO;
import com.app.monitoringapps.R;

/**
 * TODO: Add a class header comment!
 */

public class JadwalSholatAdapter extends BaseAdapter {

    JadwalSholatDTO[] mListData = new JadwalSholatDTO[5];
    Context mContext;

    public JadwalSholatAdapter(JadwalSholatDTO[] listData, Context ctx){
        mListData = listData;
        mContext = ctx;
    }

    @Override
    public int getCount() {
        return mListData.length;
    }

    @Override
    public Object getItem(int position) {
        return mListData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.adapter_sholat,null,false);

        TextView tv1 = (TextView)view.findViewById(R.id.tNSholat);
        TextView tv2 = (TextView)view.findViewById(R.id.tWSholat);

        tv1.setText(mListData[position].getName());
        tv2.setText(mListData[position].getJadwal());
        Log.d("value",mListData[position].getName()+"+"+mListData[position].getJadwal()+"-"+position);

        return view;
    }
}
