package com.app.monitoringapps.dto;

import android.widget.TableRow;

/**
 * TODO: Add a class header comment!
 */

public class SholatDTO {


    private String note,user;
    private boolean isDone;
    private TableRow row;
    private int id;
    boolean shubuh,dhuhur,ashar,maghrib,isya;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public TableRow getRow() {
        return row;
    }

    public void setRow(TableRow row) {
        this.row = row;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isShubuh() {
        return shubuh;
    }

    public void setShubuh(boolean shubuh) {
        this.shubuh = shubuh;
    }

    public boolean isDhuhur() {
        return dhuhur;
    }

    public void setDhuhur(boolean dhuhur) {
        this.dhuhur = dhuhur;
    }

    public boolean isAshar() {
        return ashar;
    }

    public void setAshar(boolean ashar) {
        this.ashar = ashar;
    }

    public boolean isMaghrib() {
        return maghrib;
    }

    public void setMaghrib(boolean maghrib) {
        this.maghrib = maghrib;
    }

    public boolean isIsya() {
        return isya;
    }

    public void setIsya(boolean isya) {
        this.isya = isya;
    }

    @Override
    public String toString() {
        return "SholatDTO{" +
                "note='" + note + '\'' +
                ", user='" + user + '\'' +
                ", isDone=" + isDone +
                ", row=" + row +
                ", id=" + id +
                ", shubuh=" + shubuh +
                ", dhuhur=" + dhuhur +
                ", ashar=" + ashar +
                ", maghrib=" + maghrib +
                ", isya=" + isya +
                '}';
    }
}
