package com.app.monitoringapps.dto;

import android.widget.TableRow;

/**
 * TODO: Add a class header comment!
 */

public class MengajiDTO {

    private String note,user;
    private boolean isDone;
    private TableRow row;
    private int id,userId;


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setRow(TableRow row) {
        this.row = row;
    }

    public TableRow getRow() {
        return row;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MengajiDTO{" +
                "note='" + note + '\'' +
                ", user='" + user + '\'' +
                ", isDone=" + isDone +
                ", row=" + row +
                ", id=" + id +
                ", userId=" + userId +
                '}';
    }
}
