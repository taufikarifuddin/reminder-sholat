package com.app.monitoringapps.dto;

import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */

public class DailyReportDTO {

    @SerializedName("user_id")
    private String user;
    private String date;

    public DailyReportDTO(String user, String date) {
        this.user = user;
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
