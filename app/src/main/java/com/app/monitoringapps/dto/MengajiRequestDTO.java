package com.app.monitoringapps.dto;

/**
 * TODO: Add a class header comment!
 */

public class MengajiRequestDTO {

    String description;
    int user_id;
    boolean is_mengaji;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public boolean isIs_mengaji() {
        return is_mengaji;
    }

    public void setIs_mengaji(boolean is_mengaji) {
        this.is_mengaji = is_mengaji;
    }
}
