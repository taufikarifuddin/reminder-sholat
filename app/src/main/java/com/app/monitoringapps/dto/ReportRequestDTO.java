package com.app.monitoringapps.dto;

import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */

public class ReportRequestDTO {
    private int year,month;

    @SerializedName("user_id")
    private int userId;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
