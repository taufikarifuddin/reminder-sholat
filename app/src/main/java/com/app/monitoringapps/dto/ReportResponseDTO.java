package com.app.monitoringapps.dto;

import com.app.monitoringapps.domain.Report;

import java.util.List;

/**
 * TODO: Add a class header comment!
 */

public class ReportResponseDTO {

    List<Report> menghafal;
    List<Report> mengaji;
    List<Report> sholat;
    String note;

    public List<Report> getMenghafal() {
        return menghafal;
    }

    public void setMenghafal(List<Report> menghafal) {
        this.menghafal = menghafal;
    }

    public List<Report> getMengaji() {
        return mengaji;
    }

    public void setMengaji(List<Report> mengaji) {
        this.mengaji = mengaji;
    }

    public List<Report> getSholat() {
        return sholat;
    }

    public void setSholat(List<Report> sholat) {
        this.sholat = sholat;
    }

    @Override
    public String toString() {
        return "ReportResponseDTO{" +
                "menghafal=" + menghafal +
                ", mengaji=" + mengaji +
                ", sholat=" + sholat +
                '}';
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }
}
