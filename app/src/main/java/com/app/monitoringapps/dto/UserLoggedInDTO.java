package com.app.monitoringapps.dto;

import com.app.monitoringapps.util.Constant;

/**
 * TODO: Add a class header comment!
 */

public class UserLoggedInDTO {

    private String email,token,name;
    private int id,role;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return Constant.AUTH_TOKEN_INDEX+token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
