package com.app.monitoringapps.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.monitoringapps.domain.SholatSchedule;
import com.app.monitoringapps.dto.JadwalSholatDTO;
import com.app.monitoringapps.listeners.UserLocationListener;
import com.app.monitoringapps.R;
import com.app.monitoringapps.activities.DirectionActivity;
import com.app.monitoringapps.adapters.JadwalSholatAdapter;
import com.app.monitoringapps.callbacks.UserLocationCallback;
import com.app.monitoringapps.util.DialogPermissionFactory;
import com.app.monitoringapps.util.SholatSharedPreference;

import org.arabeyes.itl.newmethod.MethodId;
import org.arabeyes.itl.newmethod.Prayer;
import org.arabeyes.itl.newmethod.PrayerTimes;
import org.w3c.dom.Text;

import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.app.monitoringapps.util.DialogPermissionFactory.INTERNET_LOCATION_REQUEST_CODE;

public class JadwalSholatFragment extends Fragment{

    private static final String PROGRESS_DIALOG_TITLE = "Getting your location..";
    private static final String PROGRESS_DIALOG_MESSAGE = "Please wait..";
    JadwalSholatDTO[] datas = new JadwalSholatDTO[5];

    @BindView(R.id.qiblatBtn)
    Button mQiblatBtn;

//    @BindView(R.id.gridView)
//    GridView gv;
//
//
    @BindView(R.id.timeSubuh)
    TextView subuh;

    @BindView(R.id.timeDhuhur)
    TextView duhur;

    @BindView(R.id.timeAsyar)
    TextView asyar;

    @BindView(R.id.timeIsya)
    TextView isya;

    @BindView(R.id.timeMaghrib)
    TextView magrib;

    ProgressDialog mProgressDialog;

    LocationManager mLocationManager;

    Location mLocation;

    JadwalSholatAdapter adapter;

    public JadwalSholatFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jadwal_sholat, container, false);
        ButterKnife.bind(this,view);

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setTitle(PROGRESS_DIALOG_TITLE);
        mProgressDialog.setMessage(PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.setCancelable(true);

        mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        setDatas(null);

        SholatSchedule sholatSchedule = SholatSharedPreference.getInstance().getSchedule(getContext());
        if( sholatSchedule == null ) {
            updateJadwal();
        }else{
            setToLabel(sholatSchedule.getSubuh(),sholatSchedule.getDuhur(),
                    sholatSchedule.getAsyar(),sholatSchedule.getMaghrib(),sholatSchedule.getIsya());
        }

//        PendingIntent pendingIntent = PendingIntent.getBroadcast();

        return view;
    }

    @OnClick(R.id.qiblatBtn)
    public void onClickQiblat(View view){
        startActivity(new Intent(getContext(), DirectionActivity.class));
    }

    @OnClick(R.id.refreshLocBtn)
    public void onClickUpdate(View view){
        refreshLocation();
    }

    private void updateJadwal(){
        mProgressDialog.show();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(), UserLocationListener.LOCATION_PERMISSION_DENIED_ERORR_MESSAGE, Toast.LENGTH_SHORT).show();
            mProgressDialog.dismiss();
        }else{

            mLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if( mLocation != null ) {
                Prayer calculator = new Prayer()
                        .setCalculationMethod(MethodId.EGAS)
                        .setDate(new GregorianCalendar())
                        .setLocation(mLocation.getLatitude(), mLocation.getLongitude(), 0); // lat, lng, height AMSL

                setDatas(calculator.getPrayerTimes());
                Toast.makeText(getContext(),"Mengambil lokasi terkahir anda, ketik Update Lokasi untuk memperbaharui lokasi",
                        Toast.LENGTH_SHORT).show();

                mProgressDialog.dismiss();
            }else {
                refreshLocation();
            }
        }
    }

    public void refreshLocation() {
        mProgressDialog.show();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(getContext(), UserLocationListener.LOCATION_PERMISSION_DENIED_ERORR_MESSAGE, Toast.LENGTH_SHORT).show();

            Log.d("LOCATION_PERMISSION", "Contact permissions has NOT been granted. Requesting permissions.");

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.INTERNET},
                    INTERNET_LOCATION_REQUEST_CODE);

            mProgressDialog.dismiss();
        }else{
            Log.d("LOCATION_PERMISSION", "Contact permissions has been granted.");
            mLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new UserLocationListener(new UserLocationCallback() {
                @Override
                public void getData(Location location, LocationListener listener) {
                    mLocation = location;
                    mLocationManager.removeUpdates(listener);

                    Prayer calculator = new Prayer()
                            .setCalculationMethod(MethodId.EGAS)
                            .setDate(new GregorianCalendar())
                            .setLocation(mLocation.getLatitude(), mLocation.getLongitude(), 0); // lat, lng, height AMSL

                    setDatas(calculator.getPrayerTimes());

                    mProgressDialog.dismiss();
                }
            }));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case DialogPermissionFactory.INTERNET_LOCATION_REQUEST_CODE :
                if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    refreshLocation();
                }else{
                    Toast.makeText(getContext(),"Pastikan GPS aktif / Setujui Permission yang diminta",Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setDatas(PrayerTimes time){
        if( time != null ){
            String subuh = time.getFajr().toString();
            String duhur = time.getDhuhr().toString();
            String asyar = time.getAsr().toString();
            String maghrib = time.getMaghrib().toString();
            String isya = time.getIsha().toString();

            SholatSharedPreference.getInstance().write(getContext(),subuh,duhur,asyar,maghrib,isya);

            setToLabel(subuh,duhur,asyar, maghrib,isya);

        }
    }

    private void setToLabel(String subuh, String duhur,String asyar, String maghrib,String isya){
        this.subuh.setText(subuh);
        this.duhur.setText(duhur);
        this.asyar.setText(asyar);
        this.magrib.setText(maghrib);
        this.isya.setText(isya);
    }

}
