package com.app.monitoringapps.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.monitoringapps.R;
import com.app.monitoringapps.api.BaseResponse;
import com.app.monitoringapps.api.MengajiService;
import com.app.monitoringapps.domain.Mengaji;
import com.app.monitoringapps.dto.HafalanDTO;
import com.app.monitoringapps.dto.MengajiDTO;
import com.app.monitoringapps.util.Constant;
import com.app.monitoringapps.util.DialogMengajiFactory;
import com.app.monitoringapps.util.LoadingFactory;
import com.app.monitoringapps.util.RetrofitFactory;
import com.app.monitoringapps.util.UserSharedPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HafalanFragment extends Fragment implements DialogMengajiFactory.DialogMengajiCallback {

    List<HafalanDTO> mDatas;

    @BindView(R.id.tableHafalan)
    TableLayout tableLayout;

    MengajiService service;

    ProgressDialog dialog;

    HafalanDTO currentDto;

    public HafalanFragment() {
        mDatas = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hafalan, container, false);
        ButterKnife.bind(this,view);

        service = RetrofitFactory.getClient(Constant.BASE_API_URL).create(MengajiService.class);
        dialog = LoadingFactory.getInstance().build(getContext(),"Getting information from server",Constant.PROGRESS_DIALOG_MESSAGE);

        dialog.show();
        service.hafalan(UserSharedPreference.getInstance().getUser(getContext()).getToken())
                .enqueue(new Callback<BaseResponse<List<Mengaji>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Mengaji>>> call, Response<BaseResponse<List<Mengaji>>> response) {
                        List<Mengaji> datas = response.body().getData();

                        for( Mengaji detail : datas ){
                            Log.d("data",detail.toString());
                            HafalanDTO obj = new HafalanDTO();
                            obj.setId(detail.getId());
                            obj.setDone(detail.isDone());
                            obj.setUser(detail.getUser());
                            obj.setNote(detail.getDescription());
                            obj.setRow(generateRow(obj));
                            tableLayout.addView(obj.getRow());

                            mDatas.add(obj);
                        }
                        dialog.hide();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Mengaji>>> call, Throwable t) {
                        dialog.hide();
                    }
                });

        return view;
    }

    private TableRow generateRow(final HafalanDTO dto){
        TableRow tableRow = new TableRow(getContext());
        tableRow.setBackgroundColor(Color.WHITE);
        tableRow.setPadding(10,10,10,10);
        TableRow.LayoutParams param = new TableRow.LayoutParams();
        param.span = 2;

        tableRow.setLayoutParams(param);
        tableRow.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView tv = new TextView(getContext());
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        tv.setText(dto.getUser());

        tableRow.addView(tv);

        if( !dto.isDone() ) {
            tableRow.addView(generateTextView("-"));
            tableRow.addView(generateTextView("-"));
        }else{
            tableRow.addView(generateImage());
            tableRow.addView(generateTextView(dto.getNote()));
        }
        tableRow.setClickable(true);
        tableRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserSharedPreference.getInstance().getUser(getContext()).getRole() == Constant.ROLE_STUDENT_CODE){
                    Toast.makeText(getContext(),Constant.ERROR_MESSAGE_FOR_VERIFY_STUDENT_HAVE_NO_ACCESS,Toast.LENGTH_SHORT).show();
                }else{
                    currentDto = dto;
                    DialogMengajiFactory.getInstance().create(getContext(),
                            LayoutInflater.from(getContext()),"Hafalan",currentDto.getUser(),HafalanFragment.this)
                            .show();
                    Toast.makeText(getContext(),dto.getUser(),Toast.LENGTH_SHORT).show();
                }
            }
        });

        return tableRow;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSubmit(final String desc) {
        Mengaji mengaji = new Mengaji();
        mengaji.setUserId(currentDto.getId());
        mengaji.setDescription(desc);
        mengaji.setMengaji(false);
        dialog.show();
        service.submit(mengaji, UserSharedPreference.getInstance().getUser(getContext()).getToken())
                .enqueue(new Callback<BaseResponse<HashMap<String, Object>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<HashMap<String, Object>>> call, Response<BaseResponse<HashMap<String, Object>>> response) {
                        BaseResponse<HashMap<String, Object>> data = response.body();
                        Log.d("result",response.body().toString());
                        if( !data.isError() ) {
                            Log.d("sukses","hehe");
                        }
                        dialog.hide();

                        TableRow row = currentDto.getRow();
                        row.removeView(row.getVirtualChildAt(1));
                        row.removeView(row.getVirtualChildAt(1));

                        row.addView(generateImage());
                        row.addView(generateTextView(desc));

                        currentDto.setRow(row);

                    }

                    @Override
                    public void onFailure(Call<BaseResponse<HashMap<String, Object>>> call, Throwable t) {
                        dialog.hide();
                    }
                });
    }


    private View generateImage(){
        ImageView img = new ImageView(getContext());
        img.setImageResource(R.drawable.icon_mengaji_alquran);
        img.setLayoutParams(new TableRow.LayoutParams(30,30));
        return img;
    }

    private View generateTextView(String text){
        TextView tv= new TextView(getContext());
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        tv.setText(text);
        return tv;
    }

}
