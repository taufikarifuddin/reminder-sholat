package com.app.monitoringapps.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.monitoringapps.R;
import com.app.monitoringapps.api.BaseResponse;
import com.app.monitoringapps.api.MengajiService;
import com.app.monitoringapps.domain.Mengaji;
import com.app.monitoringapps.dto.MengajiDTO;
import com.app.monitoringapps.util.Constant;
import com.app.monitoringapps.util.DialogMengajiFactory;
import com.app.monitoringapps.util.LoadingFactory;
import com.app.monitoringapps.util.RetrofitFactory;
import com.app.monitoringapps.util.UserSharedPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MengajiFragment extends Fragment implements DialogMengajiFactory.DialogMengajiCallback{

    @BindView(R.id.tableMengaji)
    TableLayout tableData;

    List<MengajiDTO> mDatas;

    MengajiService service;

    ProgressDialog dialog;

    MengajiDTO currentDto;

    public MengajiFragment() {
        mDatas = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mengaji, container, false);
        ButterKnife.bind(this,view);

        service = RetrofitFactory.getClient(Constant.BASE_API_URL).create(MengajiService.class);

        dialog = LoadingFactory.getInstance().build(getContext(),"Getting information from server",Constant.PROGRESS_DIALOG_MESSAGE);

        dialog.show();

        getData();

        return view;
    }

    private void getData(){
        service.mengaji(UserSharedPreference.getInstance().getUser(getContext())
                .getToken()).enqueue(new Callback<BaseResponse<List<Mengaji>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Mengaji>>> call, Response<BaseResponse<List<Mengaji>>> response) {
                Log.d("debug",response.toString());
                List<Mengaji> datas = response.body().getData();
                for( Mengaji detail : datas  ){
                    MengajiDTO obj = new MengajiDTO();
                    obj.setId(detail.getId());
                    obj.setDone(detail.isDone());
                    obj.setUser(detail.getUser());
                    obj.setRow(generateRow(obj));
                    tableData.addView(obj.getRow());
                    mDatas.add(obj);
                }

                dialog.hide();
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Mengaji>>> call, Throwable t) {

            }
        });
    }

    private TableRow generateRow(final MengajiDTO dto){
        TableRow tableRow = new TableRow(getContext());
        tableRow.setBackgroundColor(Color.WHITE);
        tableRow.setPadding(10,10,10,10);
        TableRow.LayoutParams param = new TableRow.LayoutParams();
        param.span = 2;

        tableRow.setLayoutParams(param);
        tableRow.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView tv = new TextView(getContext());
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        tv.setText(dto.getUser());

        tableRow.addView(tv);

        if( !dto.isDone() ) {
            TextView  blank= new TextView(getContext());
            blank.setGravity(Gravity.CENTER_HORIZONTAL);
            blank.setText("-");
            tableRow.addView(blank);
        }else{
            tableRow.addView(generateImage());
        }

        tableRow.setClickable(true);
        tableRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserSharedPreference.getInstance().getUser(getContext()).getRole() == Constant.ROLE_STUDENT_CODE){
                    Toast.makeText(getContext(),Constant.ERROR_MESSAGE_FOR_VERIFY_STUDENT_HAVE_NO_ACCESS,Toast.LENGTH_SHORT).show();
                }else{
                    currentDto = dto;
                    DialogMengajiFactory.getInstance().create(getContext(),
                            LayoutInflater.from(getContext()),"Mengaji",currentDto.getUser(),MengajiFragment.this)
                            .show();
                }

            }
        });

        return tableRow;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSubmit(String desc) {
        Mengaji mengaji = new Mengaji();
        mengaji.setUserId(currentDto.getId());
        mengaji.setDescription(desc);
        mengaji.setMengaji(true);
        dialog.show();
        service.submit(mengaji, UserSharedPreference.getInstance().getUser(getContext()).getToken())
        .enqueue(new Callback<BaseResponse<HashMap<String, Object>>>() {
            @Override
            public void onResponse(Call<BaseResponse<HashMap<String, Object>>> call, Response<BaseResponse<HashMap<String, Object>>> response) {
                BaseResponse<HashMap<String, Object>> data = response.body();
                Log.d("result",response.body().toString());
                if( !data.isError() ) {
                    Log.d("sukses","hehe");
                }
                dialog.hide();
            }

            @Override
            public void onFailure(Call<BaseResponse<HashMap<String, Object>>> call, Throwable t) {
                dialog.hide();
            }
        });

        TableRow row = currentDto.getRow();
        for (int i = 0; i < row.getChildCount(); i++) {
            View view = row.getVirtualChildAt(i);
            if( view instanceof TextView ){
                if( !((TextView)view).getText().toString().equals("-") ) continue;

                row.removeView(view);
                row.addView(generateImage());
            }
        }

        currentDto.setRow(row);
    }

    private View generateImage(){
        ImageView img = new ImageView(getContext());
        img.setImageResource(R.drawable.icon_mengaji_alquran);
        img.setLayoutParams(new TableRow.LayoutParams(30,30));
        return img;
    }

}
