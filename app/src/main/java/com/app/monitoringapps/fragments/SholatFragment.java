package com.app.monitoringapps.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.monitoringapps.R;
import com.app.monitoringapps.api.BaseResponse;
import com.app.monitoringapps.api.SholatService;
import com.app.monitoringapps.domain.Sholat;
import com.app.monitoringapps.dto.MengajiDTO;
import com.app.monitoringapps.dto.SholatDTO;
import com.app.monitoringapps.util.Constant;
import com.app.monitoringapps.util.DialogSholatFactory;
import com.app.monitoringapps.util.LoadingFactory;
import com.app.monitoringapps.util.RetrofitFactory;
import com.app.monitoringapps.util.UserSharedPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SholatFragment extends Fragment {

    @BindView(R.id.tableSholat)
    TableLayout tableLayout;

    ArrayList<SholatDTO> mDatas;

    ProgressDialog dialog;

    SholatService service;

    SholatDTO currentDto;

    public SholatFragment() {
        mDatas = new ArrayList<>();
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sholat, container, false);
        ButterKnife.bind(this,view);

        dialog = LoadingFactory.getInstance().build(getContext(),"Getting information from server", Constant.PROGRESS_DIALOG_MESSAGE);

        dialog.show();

        service = RetrofitFactory.getClient(Constant.BASE_API_URL).create(SholatService.class);

        service.sholat(UserSharedPreference.getInstance().getUser(getContext()).getToken())
                .enqueue(new Callback<BaseResponse<List<Sholat>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Sholat>>> call, Response<BaseResponse<List<Sholat>>> response) {

                        List<Sholat> datas = response.body().getData();
                        for( Sholat obj : datas ){
                            SholatDTO objDto = new SholatDTO();
                            objDto.setId(obj.getId());
                            objDto.setUser(obj.getUser());
                            objDto.setIsya(obj.isIsya());
                            objDto.setShubuh(obj.isShubuh());
                            objDto.setDhuhur(obj.isDhuhur());
                            objDto.setAshar(obj.isAshar());
                            objDto.setMaghrib(obj.isMaghrib());
                            objDto.setRow(generateRow(objDto));
                            tableLayout.addView(objDto.getRow());

                            mDatas.add(objDto);
                        }

                        dialog.hide();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Sholat>>> call, Throwable t) {
                        dialog.hide();
                    }
                });

        return view;
    }


    private TableRow generateRow(final SholatDTO dto){
        TableRow tableRow = new TableRow(getContext());
        tableRow.setBackgroundColor(Color.WHITE);
        tableRow.setPadding(10,10,10,10);
        TableRow.LayoutParams param = new TableRow.LayoutParams();
        param.span = 2;

        tableRow.setLayoutParams(param);
        tableRow.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView tv = new TextView(getContext());
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        tv.setText(dto.getUser());

        tableRow.addView(tv);

        if( !dto.isShubuh() ) {
            tableRow.addView(generateTextView());
        }else{
            tableRow.addView(generateImage());
        }

        if( !dto.isDhuhur() ) {
            tableRow.addView(generateTextView());
        }else{
            tableRow.addView(generateImage());
        }

        if( !dto.isAshar() ) {
            tableRow.addView(generateTextView());
        }else{
            tableRow.addView(generateImage());
        }

        if( !dto.isMaghrib() ) {
            tableRow.addView(generateTextView());
        }else{
            tableRow.addView(generateImage());
        }

        if( !dto.isIsya() ) {
            tableRow.addView(generateTextView());
        }else{
            tableRow.addView(generateImage());
        }


        tableRow.setClickable(true);
        tableRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(UserSharedPreference.getInstance().getUser(getContext()).getRole() == Constant.ROLE_STUDENT_CODE){
                Toast.makeText(getContext(),Constant.ERROR_MESSAGE_FOR_VERIFY_STUDENT_HAVE_NO_ACCESS,Toast.LENGTH_SHORT).show();
            }else{
                currentDto = dto;
                DialogSholatFactory.getInstance().create(getContext(), LayoutInflater.from(getContext()), dto, new DialogSholatFactory.DialogMengajiCallback() {
                    @Override
                    public void onSubmit(final boolean subuh, final boolean duhur, final boolean asyar, final boolean maghrib, final boolean isya) {

                        Sholat sholat = new Sholat();
                        sholat.setId(currentDto.getId());
                        sholat.setShubuh(subuh);
                        sholat.setDhuhur(duhur);
                        sholat.setAshar(asyar);
                        sholat.setMaghrib(maghrib);
                        sholat.setIsya(isya);

                        Log.d("sholat",sholat.toString());

                        service.submit(sholat,UserSharedPreference.getInstance().getUser(getContext())
                                .getToken()).enqueue(new Callback<BaseResponse<HashMap<String, Object>>>() {
                            @Override
                            public void onResponse(Call<BaseResponse<HashMap<String, Object>>> call, Response<BaseResponse<HashMap<String, Object>>> response) {

                                TableRow row = currentDto.getRow();

                                for (int i = 0; i < 5; i++) {
                                    row.removeView(row.getVirtualChildAt(1));
                                }

                                if( !subuh ) {
                                    row.addView(generateTextView());
                                    currentDto.setShubuh(false);
                                }else{
                                    row.addView(generateImage());
                                    currentDto.setShubuh(true);
                                }

                                if( !duhur ) {
                                    row.addView(generateTextView());
                                    currentDto.setDhuhur(false);
                                }else{
                                    row.addView(generateImage());
                                    currentDto.setDhuhur(true);
                                }

                                if( !asyar ) {
                                    row.addView(generateTextView());
                                    currentDto.setAshar(false);
                                }else{
                                    row.addView(generateImage());
                                    currentDto.setAshar(true);
                                }

                                if( !maghrib ) {
                                    row.addView(generateTextView());
                                    currentDto.setMaghrib(false);
                                }else{
                                    row.addView(generateImage());
                                    currentDto.setMaghrib(true);
                                }

                                if( !isya ) {
                                    row.addView(generateTextView());
                                    currentDto.setIsya(false);
                                }else{
                                    row.addView(generateImage());
                                    currentDto.setIsya(true);
                                }

                            }

                            @Override
                            public void onFailure(Call<BaseResponse<HashMap<String, Object>>> call, Throwable t) {
                                Log.d("error",t.getMessage());
                            }
                        });
                    }
                }).show();
            }
            }
        });

        return tableRow;
    }

    private View generateTextView(){
        TextView  blank= new TextView(getContext());
        blank.setGravity(Gravity.CENTER_HORIZONTAL);
        blank.setText("-");
        return blank;
    }

    private View generateImage(){
        ImageView img = new ImageView(getContext());
        img.setImageResource(R.drawable.icon_bintang);
        img.setLayoutParams(new TableRow.LayoutParams(30,30));
        return img;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
