package com.app.monitoringapps.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.monitoringapps.api.BaseResponse;
import com.app.monitoringapps.api.PublicFacilityService;
import com.app.monitoringapps.domain.PublicFacility;
import com.app.monitoringapps.domain.PublicFacilityCategory;
import com.app.monitoringapps.listeners.UserLocationListener;
import com.app.monitoringapps.R;
import com.app.monitoringapps.callbacks.UserLocationCallback;
import com.app.monitoringapps.util.Constant;
import com.app.monitoringapps.util.RetrofitFactory;
import com.app.monitoringapps.util.UserSharedPreference;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.monitoringapps.util.Constant.PROGRESS_DIALOG_MESSAGE;

public class LokasiFragment extends Fragment implements OnMapReadyCallback,AdapterView.OnItemSelectedListener {

    private static final String PROGRESS_DIALOG_TITLE = "Getting your location..";

    @BindView(R.id.spinner)
    Spinner mSpinner;

    Location mLocation;

    LocationManager mLocationManager;

    private List<PublicFacilityCategory> datas;

    private List<String> categories;

    private List<Marker> markers;

    private SupportMapFragment mapFragment;

    private ProgressDialog mProgressDialog;

    private PublicFacilityService service;

    private ArrayAdapter<String> adapter;

    private GoogleMap googleMap;

    public LokasiFragment() {
        categories = new ArrayList<>();
        datas = new ArrayList<>();
        markers = new ArrayList<>();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lokasi, container, false);
        ButterKnife.bind(this, view);

        if (mapFragment == null) {
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
            mapFragment.getMapAsync(this);
        }

        adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, categories);


        mSpinner.setAdapter(adapter);
        mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setTitle(PROGRESS_DIALOG_TITLE);
        mProgressDialog.setMessage(PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.setCancelable(true);

        mSpinner.setOnItemSelectedListener(this);

        getData();

        return view;
    }

    private void getData(){
        mProgressDialog.show();
        service = RetrofitFactory.getClient(Constant.BASE_API_URL).create(PublicFacilityService.class);
        service.getCategory(UserSharedPreference.getInstance().getUser(getContext()).getToken())
                .enqueue(new Callback<BaseResponse<List<PublicFacilityCategory>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<PublicFacilityCategory>>> call, Response<BaseResponse<List<PublicFacilityCategory>>> response) {
                datas = response.body().getData();

                categories.clear();

                for( PublicFacilityCategory detail : datas ){
                    categories.add(detail.getName());
                }

                adapter.notifyDataSetChanged();

          }

            @Override
            public void onFailure(Call<BaseResponse<List<PublicFacilityCategory>>> call, Throwable t) {
                Log.d("error","error");
                Log.d("errorData",t.getMessage());
            }
        });
        mProgressDialog.hide();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {

        if( this.googleMap == null ) this.googleMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(), UserLocationListener.LOCATION_PERMISSION_DENIED_ERORR_MESSAGE, Toast.LENGTH_SHORT).show();
        }else{
            mProgressDialog.show();
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new UserLocationListener(new UserLocationCallback() {
                @Override
                public void getData(Location location,LocationListener listener) {
                    mLocation = location;
                    mLocationManager.removeUpdates(listener);

                    LatLng userLoc =  new LatLng(location.getLatitude(),location.getLongitude());

                    googleMap.addMarker(new MarkerOptions()
                        .position(userLoc)
                        .title("You"));

                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLoc,15));

                    mProgressDialog.dismiss();
                }
            }));
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        changeMarker(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        changeMarker(0);
    }

    private void changeMarker(int position){
        if( datas.get(position) != null ){
            removeAllMarker();

            for(PublicFacility detail : datas.get(position).getWisata()) {
                LatLng userLoc =  new LatLng(detail.getLatitude(),detail.getLongitude());

                Marker marker = googleMap.addMarker(new MarkerOptions()
                        .position(userLoc)
                        .title(detail.getName()));

                markers.add(marker);
            }


        }
    }

    private void removeAllMarker(){
        for( Marker detail : markers ){
            detail.remove();
        }

        markers.clear();
    }

}
