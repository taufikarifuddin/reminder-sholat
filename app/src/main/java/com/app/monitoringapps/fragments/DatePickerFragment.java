package com.app.monitoringapps.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.app.monitoringapps.callbacks.DatePickerCallback;

import java.util.Calendar;
import java.util.Date;

import javax.security.auth.callback.Callback;

/**
 * TODO: Add a class header comment!
 */

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    DatePickerCallback callback;


    public void setCallback(DatePickerCallback cb) {
        this.callback = cb;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        return new DatePickerDialog(getActivity(),this,year,month,day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if( callback != null ){
            callback.setDate(new Date(year,month,dayOfMonth));
        }
    }
}
