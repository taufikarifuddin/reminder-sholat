package com.app.monitoringapps.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.monitoringapps.R;
import com.app.monitoringapps.activities.DailyReport;
import com.app.monitoringapps.activities.ReportActivity;
import com.app.monitoringapps.api.BaseResponse;
import com.app.monitoringapps.api.UserService;
import com.app.monitoringapps.domain.User;
import com.app.monitoringapps.dto.HafalanDTO;
import com.app.monitoringapps.util.Constant;
import com.app.monitoringapps.util.LoadingFactory;
import com.app.monitoringapps.util.RetrofitFactory;
import com.app.monitoringapps.util.UserSharedPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportFragment extends Fragment{

    public static String BUNDLE_USER_ID_KEY = "user_id";

    @BindView(R.id.reportTable)
    TableLayout tableLayout;

    List<String> datas;

    public ReportFragment() {
        datas = new ArrayList<>();
    }

    UserService service;

    ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_report, container, false);
        ButterKnife.bind(this,view);

        service = RetrofitFactory.getClient(Constant.BASE_API_URL).create(UserService.class);

        dialog = LoadingFactory.getInstance().build(getContext(),"Getting information from server",Constant.PROGRESS_DIALOG_MESSAGE);

        dialog.show();
        service.getAll(UserSharedPreference.getInstance().getUser(getContext()).getToken())
                .enqueue(new Callback<BaseResponse<List<User>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<User>>> call, Response<BaseResponse<List<User>>> response) {
                        List<User> users = response.body().getData();
                        Log.d("user",users.toString());
                        for( User user : users ){
                            tableLayout.addView(generateRow(user));
                        }
                        dialog.hide();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<User>>> call, Throwable t) {
                        dialog.hide();
                    }
                });

        return view;

    }

    private TableRow generateRow(final User dto){

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        TableRow tableRow = new TableRow(getContext());
        tableRow.setBackgroundColor(Color.WHITE);
        tableRow.setPadding(10,10,10,10);
        TableRow.LayoutParams param = new TableRow.LayoutParams();
        param.span = 2;

        tableRow.setLayoutParams(param);
        tableRow.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView tv = new TextView(getContext());
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        tv.setText(dto.getName());

        tableRow.addView(tv);

        ImageView img = new ImageView(getContext());
        img.setImageResource(R.drawable.button_detail_jurnal);
        img.setLayoutParams(new TableRow.LayoutParams(30,30));

        tableRow.addView(img);

        tableRow.setClickable(true);
        tableRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                builder.setTitle("Jenis Report")
                        .setItems(R.array.jenis_report, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0 :
                                        Intent daily = new Intent(getContext(), DailyReport.class);
                                        daily.putExtra(BUNDLE_USER_ID_KEY,dto.getId());
                                        startActivity(daily);
                                        break;
                                    case 1 :
                                        Intent monthly = new Intent(getContext(), ReportActivity.class);
                                        monthly.putExtra(BUNDLE_USER_ID_KEY,dto.getId());
                                        startActivity(monthly);
                                        break;
                                    default:;
                                }
                            }
                        })
                        .show();
            }
        });

        return tableRow;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

}
