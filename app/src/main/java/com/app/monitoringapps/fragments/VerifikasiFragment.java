package com.app.monitoringapps.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.monitoringapps.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VerifikasiFragment extends Fragment implements AdapterView.OnItemSelectedListener{


    @BindView(R.id.category)
    Spinner spiner;

    FragmentTransaction fragmentTransaction;

    public VerifikasiFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verifikasi, container, false);
        ButterKnife.bind(this,view);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, new String[]{"Mengaji","Menghafal","Sholat"});

        spiner.setAdapter(adapter);

        spiner.setOnItemSelectedListener(this);



        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        fragmentTransaction = getChildFragmentManager().beginTransaction();

        switch (position){
            case 0 :
                fragmentTransaction.replace(R.id.detailFrame,new MengajiFragment());
                break;
            case 1 :
                fragmentTransaction.replace(R.id.detailFrame,new HafalanFragment());
                break;
            case 2 :
                fragmentTransaction.replace(R.id.detailFrame,new SholatFragment());
                break;
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
