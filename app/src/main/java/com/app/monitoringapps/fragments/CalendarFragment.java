package com.app.monitoringapps.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;

import com.app.monitoringapps.R;

import org.arabeyes.itl.hijri.ConvertedDate;
import org.arabeyes.itl.hijri.Hijri;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalendarFragment extends Fragment implements CalendarView.OnDateChangeListener{

    Hijri calculator = new Hijri(null);

    ConvertedDate date;

    @BindView(R.id.calendarView)
    CalendarView calendarView;

    @BindView(R.id.tHijCalendar)
    TextView tHijCalendar;

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        ButterKnife.bind(this,view);

        calendarView.setOnDateChangeListener(this);


        Calendar cal = Calendar.getInstance();

        convertToHijr(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DATE));

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
        convertToHijr(year,month,dayOfMonth);
    }

    private void convertToHijr(int year,int month,int dayOfMonth){

        date = calculator.hDate(dayOfMonth, month + 1, year);
        tHijCalendar.setText(date.format("EEEE, d MMMM yyyy G"));
    }
}
