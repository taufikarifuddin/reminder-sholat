package com.app.monitoringapps.api;

import com.app.monitoringapps.activities.DailyReport;
import com.app.monitoringapps.dto.DailyReportDTO;
import com.app.monitoringapps.dto.DailyReportResponseDTO;
import com.app.monitoringapps.dto.ReportRequestDTO;
import com.app.monitoringapps.dto.ReportResponseDTO;
import com.app.monitoringapps.dto.SholatDTO;
import com.app.monitoringapps.util.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * TODO: Add a class header comment!
 */

public interface ReportService {

    @POST("report/index")
    Call<BaseResponse<ReportResponseDTO>> report(@Header(Constant.AUTH_HEADER_REQUEST) String token
                                                        , @Body ReportRequestDTO data);

    @POST("report/daily")
    Call<BaseResponse<DailyReportResponseDTO>> dailyReport(@Header(Constant.AUTH_HEADER_REQUEST) String token
                                                        , @Body DailyReportDTO data);
}
