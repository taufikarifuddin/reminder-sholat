package com.app.monitoringapps.api;

import com.app.monitoringapps.domain.Mengaji;
import com.app.monitoringapps.domain.PublicFacilityCategory;
import com.app.monitoringapps.dto.MengajiDTO;
import com.app.monitoringapps.dto.MengajiRequestDTO;
import com.app.monitoringapps.util.Constant;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * TODO: Add a class header comment!
 */

public interface MengajiService {

    @POST("mengaji/submit")
    Call<BaseResponse<HashMap<String,Object>>> submit(@Body Mengaji data,
                                                      @Header(Constant.AUTH_HEADER_REQUEST) String token);
    @GET("mengaji/mengaji")
    Call<BaseResponse<List<Mengaji>>> mengaji(@Header(Constant.AUTH_HEADER_REQUEST) String token);
//    Call<BaseResponse<List<Mengaji>>> mengaji(@Header(Constant.AUTH_HEADER_REQUEST) String token);

    @GET("mengaji/hafalan")
    Call<BaseResponse<List<Mengaji>>> hafalan(@Header(Constant.AUTH_HEADER_REQUEST) String token);

}
