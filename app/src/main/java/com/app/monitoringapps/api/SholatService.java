package com.app.monitoringapps.api;

import com.app.monitoringapps.domain.Mengaji;
import com.app.monitoringapps.domain.Sholat;
import com.app.monitoringapps.dto.MengajiRequestDTO;
import com.app.monitoringapps.dto.SholatDTO;
import com.app.monitoringapps.util.Constant;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * TODO: Add a class header comment!
 */

public interface SholatService {

    @POST("sholat/submit")
    Call<BaseResponse<HashMap<String,Object>>> submit(@Body Sholat data,
                                                      @Header(Constant.AUTH_HEADER_REQUEST) String token);
    @GET("sholat/index")
    Call<BaseResponse<List<Sholat>>> sholat(@Header(Constant.AUTH_HEADER_REQUEST) String token);

}
