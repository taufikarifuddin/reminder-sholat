package com.app.monitoringapps.api;

import com.app.monitoringapps.domain.Note;
import com.app.monitoringapps.util.Constant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * TODO: Add a class header comment!
 */

public interface NoteService {

    @POST("note/index")
    Call<BaseResponse<Object>> submitData(@Header(Constant.AUTH_HEADER_REQUEST) String token, @Body Note note);

}
