package com.app.monitoringapps.api;

import com.app.monitoringapps.domain.PublicFacilityCategory;
import com.app.monitoringapps.util.Constant;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

/**
 * TODO: Add a class header comment!
 */

public interface PublicFacilityService {

    @GET("public-facility/get-category")
    Call<BaseResponse<List<PublicFacilityCategory>>> getCategory(@Header(Constant.AUTH_HEADER_REQUEST) String token);
}
