package com.app.monitoringapps.api;

import com.app.monitoringapps.domain.User;
import com.app.monitoringapps.util.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * TODO: Add a class header comment!
 */

public interface UserService {

    @POST("user/index")
    Call<BaseResponse<List<User>>> getAll(@Header(Constant.AUTH_HEADER_REQUEST) String token);
}
