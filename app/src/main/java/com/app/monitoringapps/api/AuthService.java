package com.app.monitoringapps.api;

import com.app.monitoringapps.dto.LoginDTO;
import com.app.monitoringapps.util.Constant;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * TODO: Add a class header comment!
 */

public interface AuthService {

    @POST("auth/login")
    Call<BaseResponse<HashMap<String,String>>> login(@Body  LoginDTO dto);

    @POST("auth/logout")
    Call<BaseResponse<Object>> logout(@Header(Constant.AUTH_HEADER_REQUEST) String token);

}
