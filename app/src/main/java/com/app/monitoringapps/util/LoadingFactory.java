package com.app.monitoringapps.util;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * TODO: Add a class header comment!
 */

public class LoadingFactory {
    private static final LoadingFactory ourInstance = new LoadingFactory();

    public static LoadingFactory getInstance() {
        return ourInstance;
    }
    private ProgressDialog mProgressDialog;

    private LoadingFactory() {

    }

    public ProgressDialog build(Context ctx,String title,String message){
        mProgressDialog = new ProgressDialog(ctx);
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(true);

        return mProgressDialog;
    }

}
