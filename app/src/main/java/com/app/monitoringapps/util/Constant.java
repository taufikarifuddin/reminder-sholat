package com.app.monitoringapps.util;

/**
 * TODO: Add a class header comment!
 */

public interface Constant {

    String BASE_API_URL = "http://192.168.1.3/project/reminder/web/api-v1/";
//    String BASE_API_URL = "http://demo.ta-dev.com/reminder/web/api-v1/";
    int SUCCESS_CODE = 200;

    String RESPONSE_INDEX_STATUS = "status";
    String RESPONSE_INDEX_DATA = "data";
    String RESPONSE_INDEX_MESSAGE = "message";
    String RESPONSE_INDEX_ERROR = "error";

    String AUTH_TOKEN_INDEX = "Bearer ";

    String ERROR_MESSAGE_FOR_VERIFY_STUDENT_HAVE_NO_ACCESS = "Hanya Orantua dan Guru yang dapat melakukan verifikasi Ibadah";

    int ROLE_ADMIN_CODE = 0;
    int ROLE_TEACHER_CODE = 1;
    int ROLE_STUDENT_CODE = 2;
    int ROLE_PARENT_CODE = 3;

    String AUTH_HEADER_REQUEST = "Authorization";


    String PROGRESS_DIALOG_MESSAGE = "Please wait..";
}
