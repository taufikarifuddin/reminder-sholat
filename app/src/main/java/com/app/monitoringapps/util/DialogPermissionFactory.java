package com.app.monitoringapps.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * TODO: Add a class header comment!
 */

public class DialogPermissionFactory {

    private static DialogPermissionFactory factory = new DialogPermissionFactory();
    public static final int INTERNET_LOCATION_REQUEST_CODE = 123;


    private DialogPermissionFactory(){

    }

    public static DialogPermissionFactory getInstance(){
        return factory;
    }

    public Dialog build(Context ctx, String message, final DialogPermissionFactory.OnSubmitPermission callback){
        return new AlertDialog.Builder(ctx)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.submit();
                    }
                }).setNegativeButton(android.R.string.cancel, null)
                .create();
    }

    public interface OnSubmitPermission{
        void submit();
    }

}
