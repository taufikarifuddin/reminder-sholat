package com.app.monitoringapps.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.app.monitoringapps.R;

import org.w3c.dom.Text;

/**
 * TODO: Add a class header comment!
 */

public class DialogMengajiFactory {

    private static DialogMengajiFactory dialogFactory = new DialogMengajiFactory();

    private DialogMengajiFactory(){

    }

    public static DialogMengajiFactory getInstance(){ return dialogFactory; }

    public AlertDialog  create(Context ctx, LayoutInflater inflater,String title,String namaMurid,final DialogMengajiCallback callback){

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View view= inflater.inflate(R.layout.fragment_dialog_mengaji,null);

        final EditText editText = (EditText)view.findViewById(R.id.desc);
        final TextView label = (TextView)view.findViewById(R.id.namaMurid);
        label.setText(namaMurid);

        builder.setTitle(title);

        builder.setView(view)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onSubmit(editText.getText().toString());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }


    public interface  DialogMengajiCallback{

        void onSubmit(String desc);

    }

}
