package com.app.monitoringapps.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.app.monitoringapps.R;
import com.app.monitoringapps.dto.SholatDTO;

/**
 * TODO: Add a class header comment!
 */

public class DialogSholatFactory {

    private static final DialogSholatFactory factory = new DialogSholatFactory();

    private  DialogSholatFactory(){

    }

    public static DialogSholatFactory getInstance(){
        return factory;
    }



    public AlertDialog create(Context ctx, LayoutInflater inflater, SholatDTO dto, final DialogSholatFactory.DialogMengajiCallback callback){

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View view= inflater.inflate(R.layout.fragment_dialog_sholat,null);

        final TextView label = (TextView)view.findViewById(R.id.namaMurid);
        label.setText(dto.getUser());

        final CheckBox subuh = (CheckBox) view.findViewById(R.id.subuh);
        final CheckBox duhur = (CheckBox) view.findViewById(R.id.duhur);
        final CheckBox asyar = (CheckBox) view.findViewById(R.id.asyar);
        final CheckBox maghrib = (CheckBox) view.findViewById(R.id.maghrib);
        final CheckBox isya = (CheckBox) view.findViewById(R.id.isya);

        if( dto.isShubuh() ){
            subuh.setChecked(true);
        }

        if( dto.isDhuhur() ){
            duhur.setChecked(true);
        }

        if( dto.isAshar() ){
            asyar.setChecked(true);
        }

        if( dto.isMaghrib() ){
            maghrib.setChecked(true);
        }

        if( dto.isIsya() ){
            isya.setChecked(true);
        }


        builder.setTitle("Submit Sholat");

        builder.setView(view)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onSubmit(subuh.isChecked(),duhur.isChecked(),asyar.isChecked(),
                                maghrib.isChecked(),isya.isChecked());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }

    public interface  DialogMengajiCallback{


        void onSubmit(boolean subuh,boolean duhur, boolean asyar, boolean maghrib, boolean isya);

    }


}
