package com.app.monitoringapps.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.monitoringapps.domain.SholatSchedule;
import com.app.monitoringapps.dto.SholatDTO;
import com.app.monitoringapps.dto.UserLoggedInDTO;

/**
 * TODO: Add a class header comment!
 */

public class SholatSharedPreference {

    private static final SholatSharedPreference ourInstance = new SholatSharedPreference();
    private static final String SHOLAT_SHARED_PREFERENCES = "sholat_shared_preferences";
    private static final String PREFIX_INDEX_DATA = "SHOLAT_";
    private static final String INDEX_SUBUH = PREFIX_INDEX_DATA+"SUBUH";
    private static final String INDEX_DUHUR = PREFIX_INDEX_DATA+"DUHUR";
    private static final String INDEX_ASYAR = PREFIX_INDEX_DATA+"ASYAR";
    private static final String INDEX_MAGHRIB = PREFIX_INDEX_DATA+"MAGHRIB";
    private static final String INDEX_ISYA = PREFIX_INDEX_DATA+"ISYA";


    public static SholatSharedPreference getInstance() {
        return ourInstance;
    }

    private SholatSharedPreference() {

    }

    public void write(Context ctx, String subuh, String duhur,String asyar, String maghrib,String isya){
        if( subuh != null
                && duhur != null
                && asyar != null
                && maghrib != null
                && isya != null){

            SharedPreferences.Editor editor = getEditor(ctx);
            editor.putString(INDEX_SUBUH,subuh);
            editor.putString(INDEX_DUHUR,duhur);
            editor.putString(INDEX_ASYAR,asyar);
            editor.putString(INDEX_MAGHRIB,maghrib);
            editor.putString(INDEX_ISYA,isya);
            editor.commit();

        }
    }

    public SholatSchedule getSchedule(Context ctx){

        SharedPreferences preferences = getSharedPreferenceData(ctx);

        String subuh = preferences.getString(INDEX_SUBUH,null);
        String duhur = preferences.getString(INDEX_DUHUR,null);
        String asyar = preferences.getString(INDEX_ASYAR,null);
        String maghrib = preferences.getString(INDEX_MAGHRIB,null);
        String isya = preferences.getString(INDEX_ISYA,null);


        if( subuh == null
                || duhur == null
                || asyar == null
                || maghrib == null
                || isya == null){

            return null;

        }

        SholatSchedule sholat = new SholatSchedule();
        sholat.setSubuh(subuh);
        sholat.setDuhur(duhur);
        sholat.setAsyar(asyar);
        sholat.setMaghrib(maghrib);
        sholat.setIsya(isya);

        return sholat;
    }


    private SharedPreferences getSharedPreferenceData(Context ctx){
        return ctx.getSharedPreferences(SHOLAT_SHARED_PREFERENCES,Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor(Context ctx){
        SharedPreferences sharedPreferences = getSharedPreferenceData(ctx);
        return sharedPreferences.edit();
    }


}
