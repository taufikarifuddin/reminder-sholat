package com.app.monitoringapps.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.monitoringapps.dto.UserLoggedInDTO;

/**
 * TODO: Add a class header comment!
 */

public class UserSharedPreference {
    private static final UserSharedPreference ourInstance = new UserSharedPreference();
    private static final String USER_SHARED_PREFERENCES = "user_shared_preferences";
    private static final String PREFIX_INDEX_DATA = "REMINDER_";
    private static final String INDEX_NAME = PREFIX_INDEX_DATA+"NAME";
    private static final String INDEX_EMAIL = PREFIX_INDEX_DATA+"EMAIL";
    private static final String INDEX_ID = PREFIX_INDEX_DATA+"ID";
    private static final String INDEX_TOKEN = PREFIX_INDEX_DATA+"TOKEN";
    private static final String INDEX_ROLE = PREFIX_INDEX_DATA+"ROLE";


    public static UserSharedPreference getInstance() {
        return ourInstance;
    }

    private UserSharedPreference() {

    }

    public void write(Context ctx,String name,String email,int id,String token,int role){
        SharedPreferences.Editor editor = getEditor(ctx);
        editor.putString(INDEX_NAME,name);
        editor.putString(INDEX_EMAIL,email);
        editor.putInt(INDEX_ID,id);
        editor.putString(INDEX_TOKEN,token);
        editor.putInt(INDEX_ROLE,role);
        editor.commit();
    }

    public UserLoggedInDTO getUser(Context ctx){

        SharedPreferences preferences = getSharedPreferenceData(ctx);

        String name = preferences.getString(INDEX_NAME,null);
        String token = preferences.getString(INDEX_TOKEN,null);
        String email = preferences.getString(INDEX_EMAIL,null);
        int role = preferences.getInt(INDEX_ROLE,-1);
        int id = preferences.getInt(INDEX_ID,-1);

        if( name == null
                || token == null
                || email == null
                || role == -1
                || id == -1){

            return null;

        }

        UserLoggedInDTO user = new UserLoggedInDTO();
        user.setId(id);
        user.setRole(role);
        user.setEmail(email);
        user.setToken(token);
        user.setName(name);

        return user;
    }

    public void removeAll(Context ctx){
        SharedPreferences.Editor editor = getEditor(ctx);
        editor.remove(INDEX_ID);
        editor.remove(INDEX_ROLE);
        editor.remove(INDEX_EMAIL);
        editor.remove(INDEX_TOKEN);
        editor.remove(INDEX_NAME);
        editor.commit();
    }

    public boolean isLoggedIn(Context ctx){
        return getUser(ctx) != null;
    }

    private SharedPreferences getSharedPreferenceData(Context ctx){
        return ctx.getSharedPreferences(USER_SHARED_PREFERENCES,Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor(Context ctx){
        SharedPreferences sharedPreferences =
                ctx.getSharedPreferences(USER_SHARED_PREFERENCES,Context.MODE_PRIVATE);
        return sharedPreferences.edit();
    }
}
