package com.app.monitoringapps.util;

import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * TODO: Add a class header comment!
 */

public class RetrofitFactory {

    private static Retrofit retrofit;

    public static Retrofit getClient(String baseUrl){
        if( retrofit != null ){
            return  retrofit;
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

}
