package com.app.monitoringapps.domain;

/**
 * TODO: Add a class header comment!
 */

public class Sholat {
    private String note,user;
    private int id;
    boolean shubuh,dhuhur,ashar,maghrib,isya;


    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isShubuh() {
        return shubuh;
    }

    public void setShubuh(boolean shubuh) {
        this.shubuh = shubuh;
    }

    public boolean isDhuhur() {
        return dhuhur;
    }

    public void setDhuhur(boolean dhuhur) {
        this.dhuhur = dhuhur;
    }

    public boolean isAshar() {
        return ashar;
    }

    public void setAshar(boolean ashar) {
        this.ashar = ashar;
    }

    public boolean isMaghrib() {
        return maghrib;
    }

    public void setMaghrib(boolean maghrib) {
        this.maghrib = maghrib;
    }

    public boolean isIsya() {
        return isya;
    }

    public void setIsya(boolean isya) {
        this.isya = isya;
    }

    @Override
    public String toString() {
        return "Sholat{" +
                "note='" + note + '\'' +
                ", user='" + user + '\'' +
                ", id=" + id +
                ", shubuh=" + shubuh +
                ", dhuhur=" + dhuhur +
                ", ashar=" + ashar +
                ", maghrib=" + maghrib +
                ", isya=" + isya +
                '}';
    }
}
