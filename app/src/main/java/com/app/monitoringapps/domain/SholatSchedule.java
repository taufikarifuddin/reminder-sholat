package com.app.monitoringapps.domain;

/**
 * TODO: Add a class header comment!
 */

public class SholatSchedule {

    private String subuh,duhur,asyar,maghrib,isya;

    public String getSubuh() {
        return subuh;
    }

    public void setSubuh(String subuh) {
        this.subuh = subuh;
    }

    public String getDuhur() {
        return duhur;
    }

    public void setDuhur(String duhur) {
        this.duhur = duhur;
    }

    public String getAsyar() {
        return asyar;
    }

    public void setAsyar(String asyar) {
        this.asyar = asyar;
    }

    public String getMaghrib() {
        return maghrib;
    }

    public void setMaghrib(String maghrib) {
        this.maghrib = maghrib;
    }

    public String getIsya() {
        return isya;
    }

    public void setIsya(String isya) {
        this.isya = isya;
    }
}
