package com.app.monitoringapps.domain;

import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */

public class Note {

    private String userFrom,note;

    @SerializedName("user_id")
    private int userId;

    private int month,year;

    public String getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(String userFrom) {
        this.userFrom = userFrom;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Note{" +
                "userFrom='" + userFrom + '\'' +
                ", note='" + note + '\'' +
                ", userId=" + userId +
                ", month=" + month +
                ", year=" + year +
                '}';
    }
}
