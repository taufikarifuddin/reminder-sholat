package com.app.monitoringapps.domain;

/**
 * TODO: Add a class header comment!
 */

public class Report {

    private int userId,total;
    private String date;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Report{" +
                "userId=" + userId +
                ", total=" + total +
                ", date='" + date + '\'' +
                '}';
    }
}
