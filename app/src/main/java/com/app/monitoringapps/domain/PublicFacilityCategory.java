package com.app.monitoringapps.domain;

import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Add a class header comment!
 */

public class PublicFacilityCategory {

    private int id;
    private String name;
    List<PublicFacility> wisata = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PublicFacility> getWisata() {
        return wisata;
    }

    public void setWisata(List<PublicFacility> wisata) {
        this.wisata = wisata;
    }

    @Override
    public String toString() {
        return "PublicFacilityCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", wisata=" + wisata +
                '}';
    }
}
