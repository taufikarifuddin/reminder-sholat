package com.app.monitoringapps.domain;

import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */

public class Mengaji {

    @SerializedName("user_id")
    private int userId;

    private String note,user,description;
    private int id;

    @SerializedName("is_mengaji")
    private boolean mengaji;
    
    private  boolean isDone;

    public void setDone(boolean done) {
        isDone = done;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isMengaji() {
        return mengaji;
    }

    public void setMengaji(boolean mengaji) {
        this.mengaji = mengaji;
    }

    @Override
    public String toString() {
        return "Mengaji{" +
                "userId=" + userId +
                ", note='" + note + '\'' +
                ", user='" + user + '\'' +
                ", description='" + description + '\'' +
                ", id=" + id +
                ", mengaji=" + mengaji +
                ", isDone=" + isDone +
                '}';
    }
}
