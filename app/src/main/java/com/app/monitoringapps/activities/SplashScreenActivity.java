package com.app.monitoringapps.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.app.monitoringapps.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreenActivity extends AppCompatActivity {

    private static final int DELAY_IN_MILIS = 2000;

    @BindView(R.id.splashscreen_title)
    TextView title;

    @BindView(R.id.splashscreen_subtitle)
    TextView subTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        Typeface fontStyle = Typeface.createFromAsset(getAssets(),getString(R.string.splashscreen_font));

        title.setTypeface(fontStyle);
        subTitle.setTypeface(fontStyle);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this,LoginActivity.class));
                SplashScreenActivity.this.finish();
            }
        },DELAY_IN_MILIS);
    }
}
