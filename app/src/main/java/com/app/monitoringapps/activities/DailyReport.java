package com.app.monitoringapps.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.monitoringapps.R;
import com.app.monitoringapps.api.BaseResponse;
import com.app.monitoringapps.api.ReportService;
import com.app.monitoringapps.dto.DailyReportDTO;
import com.app.monitoringapps.dto.DailyReportResponseDTO;
import com.app.monitoringapps.fragments.ReportFragment;
import com.app.monitoringapps.util.Constant;
import com.app.monitoringapps.util.LoadingFactory;
import com.app.monitoringapps.util.RetrofitFactory;
import com.app.monitoringapps.util.UserSharedPreference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Callback;
import retrofit2.Response;

public class DailyReport extends AppCompatActivity {

    int userId;

    @BindView(R.id.dailyDatepicker)
    EditText datePicker;

    DatePickerDialog datePickerDialog;

    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    ReportService service;

    @BindView(R.id.rowShubuh)
    TableRow rowShubuh;

    @BindView(R.id.rowDhuhur)
    TableRow rowDhuhur;

    @BindView(R.id.rowAsyar)
    TableRow rowAsyar;

    @BindView(R.id.rowMaghrib)
    TableRow rowMaghrib;

    @BindView(R.id.rowIsya)
    TableRow rowIsya;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_report);
        ButterKnife.bind(this);
        datePicker.setKeyListener(null);

        dialog = LoadingFactory.getInstance().build(this,"Getting information from server",Constant.PROGRESS_DIALOG_MESSAGE);

        Date date = new Date();
        String formattedDate = dateFormat.format(date);
        datePicker.setText(formattedDate);

        Calendar cal = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_DARK, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                datePicker.setText(dayOfMonth+"-"+month+"-"+year);
                getData(new DailyReportDTO(String.valueOf(userId),datePicker.getText().toString()));
            }
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));


        userId = getIntent().getIntExtra(ReportFragment.BUNDLE_USER_ID_KEY,0);
        Log.d("userID",String.valueOf(String.valueOf(userId)));

        service = RetrofitFactory.getClient(Constant.BASE_API_URL).create(ReportService.class);

        if( userId != 0 ){
            getData(new DailyReportDTO(String.valueOf(userId),datePicker.getText().toString()));
        }

    }

    private void getData(DailyReportDTO dto){
        dialog.show();
        service.dailyReport(UserSharedPreference.getInstance().getUser(this).getToken(),dto)
                .enqueue(new Callback<BaseResponse<DailyReportResponseDTO>>() {
                    @Override
                    public void onResponse(retrofit2.Call<BaseResponse<DailyReportResponseDTO>> call, Response<BaseResponse<DailyReportResponseDTO>> response) {
                        DailyReportResponseDTO result = response.body().getData();
                        setData(result);
                    }

                    @Override
                    public void onFailure(retrofit2.Call<BaseResponse<DailyReportResponseDTO>> call, Throwable t) {

                    }
                });

    }

    @OnClick(R.id.dailyDatepicker)
    public void onDailyDatePickerClick(View view){
        datePickerDialog.show();
    }

    public void setData(DailyReportResponseDTO dto){
        dialog.dismiss();
        if( rowShubuh.getChildCount() == 2){
            rowShubuh.removeView( rowShubuh.getVirtualChildAt(1) );
        }

        if( dto.isShubuh() ){
            rowShubuh.addView(generateImage());
        }else{
            rowShubuh.addView(generateTextView());
        }

        if( rowDhuhur.getChildCount() == 2){
            rowDhuhur.removeView( rowDhuhur.getVirtualChildAt(1) );
        }

        if( dto.isDhuhur() ){
            rowDhuhur.addView(generateImage());
        }else{
            rowDhuhur.addView(generateTextView());
        }

        if( rowAsyar.getChildCount() == 2){
            rowAsyar.removeView( rowAsyar.getVirtualChildAt(1) );
        }

        if( dto.isAshar() ){
            rowAsyar.addView(generateImage());
        }else{
            rowAsyar.addView(generateTextView());
        }

        if( rowMaghrib.getChildCount() == 2){
            rowMaghrib.removeView( rowMaghrib.getVirtualChildAt(1) );
        }

        if( dto.isMaghrib() ){
            rowMaghrib.addView(generateImage());
        }else{
            rowMaghrib.addView(generateTextView());
        }

        if( rowIsya.getChildCount() == 2){
            rowIsya.removeView( rowIsya.getVirtualChildAt(1) );
        }

        if( dto.isIsya() ){
            rowIsya.addView(generateImage());
        }else{
            rowIsya.addView(generateTextView());
        }
    }

    private View generateTextView(){
        TextView blank= new TextView(this);
        blank.setGravity(Gravity.CENTER_HORIZONTAL);
        blank.setText("-");
        return blank;
    }

    private View generateImage(){
        ImageView img = new ImageView(this);
        img.setImageResource(R.drawable.icon_bintang);
        img.setLayoutParams(new TableRow.LayoutParams(30,30));
        return img;
    }
}
