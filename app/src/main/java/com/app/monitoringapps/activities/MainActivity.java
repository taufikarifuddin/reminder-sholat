package com.app.monitoringapps.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.app.monitoringapps.R;
import com.app.monitoringapps.api.AuthService;
import com.app.monitoringapps.api.BaseResponse;
import com.app.monitoringapps.fragments.CalendarFragment;
import com.app.monitoringapps.fragments.JadwalSholatFragment;
import com.app.monitoringapps.fragments.LokasiFragment;
import com.app.monitoringapps.fragments.ReportFragment;
import com.app.monitoringapps.fragments.VerifikasiFragment;
import com.app.monitoringapps.util.Constant;
import com.app.monitoringapps.util.LoadingFactory;
import com.app.monitoringapps.util.RetrofitFactory;
import com.app.monitoringapps.util.UserSharedPreference;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final String DIALOG_LOGOUT_TITLE = "Logout";
    private final String DIALOG_LOGOUT_MESSAGE = "Apakah anda yakin ingin keluar ? ";

    @BindView(R.id.bottomBar)
    BottomBar bottomBar;

    FragmentTransaction fragmentTransaction;

    private AlertDialog.Builder logoutDialog;

    ProgressDialog progressDialog;

    private AuthService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        progressDialog = LoadingFactory.getInstance().build(this,"Logout", Constant.PROGRESS_DIALOG_MESSAGE);
        service = RetrofitFactory.getClient(Constant.BASE_API_URL).create(AuthService.class);


        if( UserSharedPreference.getInstance().isLoggedIn(this)){
            Toast.makeText(this,UserSharedPreference.getInstance().getUser(this).getName(),Toast.LENGTH_SHORT).show();
        }

        logoutDialog = new AlertDialog.Builder(this);
        logoutDialog.setTitle(DIALOG_LOGOUT_TITLE);
        logoutDialog.setMessage(DIALOG_LOGOUT_MESSAGE);
        logoutDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        logoutDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressDialog.show();
                 service.logout(UserSharedPreference.getInstance().getUser(MainActivity.this).getToken())
                        .enqueue(new Callback<BaseResponse<Object>>() {
                            @Override
                            public void onResponse(Call<BaseResponse<Object>> call, Response<BaseResponse<Object>> response) {
                                Log.d("result",response.body().toString());
                                progressDialog.dismiss();
                                UserSharedPreference.getInstance().removeAll(MainActivity.this);
                                MainActivity.this.finish();
                            }

                            @Override
                            public void onFailure(Call<BaseResponse<Object>> call, Throwable t) {

                            }
                        });
            }
        });

        bottomBar.setOnTabSelectListener(bottomBarListener);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch( item.getItemId() ){
            case R.id.actionbar_logout:
                logoutDialog.show();
                return true;
            default: super.onOptionsItemSelected(item);
        }
        return false;
    }

    OnTabSelectListener bottomBarListener = new OnTabSelectListener() {
        @Override
        public void onTabSelected(@IdRes int tabId) {

            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Fragment fragmentDetail = null;
            switch(tabId){
                case R.id.tab_adzan :
                    fragmentDetail = new JadwalSholatFragment();
                    break;
                case R.id.tab_calender :
                    fragmentDetail = new CalendarFragment();
                    break;
                case R.id.tab_lokasi :
                    fragmentDetail = new LokasiFragment();
                    break;
                case R.id.tab_verifikasi :
                    fragmentDetail = new VerifikasiFragment();
                    break;
                case R.id.tab_report :
                    fragmentDetail = new ReportFragment();
                    break;
                default :
                    fragmentDetail = new LokasiFragment();
                    break;
            }

            fragmentTransaction.replace(R.id.fragment,fragmentDetail);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }
    };
}
