package com.app.monitoringapps.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.app.monitoringapps.R;
import com.app.monitoringapps.api.AuthService;
import com.app.monitoringapps.api.BaseResponse;
import com.app.monitoringapps.dto.LoginDTO;
import com.app.monitoringapps.util.Constant;
import com.app.monitoringapps.util.RetrofitFactory;
import com.app.monitoringapps.util.UserSharedPreference;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    private final String LOGIN_ERROR_MSG = "Username / Password salah / Id telah digunakan di device orang lain";
    private final String LOGIN_SUCCESS_MSG = "Anda berhasil login";
    private final String PROGRESS_DIALOG_TITLE = "Loading";
    private final String PROGRESS_DIALOG_MESSAGE = "Please wait...";
    private AuthService service;

    private ProgressDialog mProgressDialog;

    @BindView(R.id.login_username)
    TextView mUsername;

    @BindView(R.id.login_password)
    TextView mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(PROGRESS_DIALOG_TITLE);
        mProgressDialog.setMessage(PROGRESS_DIALOG_MESSAGE);
        mProgressDialog.setCancelable(false);

        Retrofit retrofit = RetrofitFactory.getClient(Constant.BASE_API_URL);

        service = retrofit.create(AuthService.class);

        if( UserSharedPreference.getInstance().isLoggedIn(this) ){
            goToMainActivity();
        }

    }

    @OnClick(R.id.login_btn)
    public void onClickSubmit(View view){
        final String username = mUsername.getText().toString();
        final String password = mPassword.getText().toString();

        mProgressDialog.show();
        service.login(new LoginDTO(username,password)).enqueue(new Callback<BaseResponse<HashMap<String, String>>>() {
            @Override
            public void onResponse(Call<BaseResponse<HashMap<String, String>>> call, Response<BaseResponse<HashMap<String, String>>> response) {
                BaseResponse<HashMap<String, String>> data = response.body();
                HashMap<String,String> dataAuth = data.getData();
                Log.d("result",data.toString());
                if( !data.isError() ){
                    UserSharedPreference.getInstance()
                            .write(LoginActivity.this,dataAuth.get("name"),
                            dataAuth.get("email"),Integer.parseInt(dataAuth.get("id"))
                                    ,dataAuth.get("token"),Integer.parseInt(dataAuth.get("role")));

                    Log.d("isLoggedIn",Boolean.toString(UserSharedPreference.getInstance()
                            .isLoggedIn(LoginActivity.this)));

                    goToMainActivity();
                    Toast.makeText(LoginActivity.this,LOGIN_SUCCESS_MSG,Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this,LOGIN_ERROR_MSG,Toast.LENGTH_SHORT).show();
                }
                mProgressDialog.hide();
            }

            @Override
            public void onFailure(Call<BaseResponse<HashMap<String, String>>> call, Throwable t) {
                mProgressDialog.hide();
                Toast.makeText(LoginActivity.this,LOGIN_ERROR_MSG,Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void goToMainActivity(){
        startActivity(new Intent(this,MainActivity.class));
    }
}
