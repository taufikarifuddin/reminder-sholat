package com.app.monitoringapps.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.monitoringapps.R;
import com.app.monitoringapps.api.BaseResponse;
import com.app.monitoringapps.api.NoteService;
import com.app.monitoringapps.api.ReportService;
import com.app.monitoringapps.domain.Mengaji;
import com.app.monitoringapps.domain.Note;
import com.app.monitoringapps.domain.Report;
import com.app.monitoringapps.dto.ReportRequestDTO;
import com.app.monitoringapps.dto.ReportResponseDTO;
import com.app.monitoringapps.fragments.DatePickerFragment;
import com.app.monitoringapps.fragments.ReportFragment;
import com.app.monitoringapps.util.Constant;
import com.app.monitoringapps.util.LoadingFactory;
import com.app.monitoringapps.util.RetrofitFactory;
import com.app.monitoringapps.util.UserSharedPreference;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportActivity extends AppCompatActivity {

    @BindView(R.id.chart)
    BarChart barChart;

    ArrayList<BarEntry> mengaji;
    ArrayList<BarEntry> hafalan;
    ArrayList<BarEntry> sholat;

    ArrayList<String> labels;
    int userId;

    @BindView(R.id.datepicker)
    EditText datepicker;

    @BindView(R.id.editNote)
    EditText editNote;

    @BindView(R.id.btnSubmit)
    Button btn;

    @BindView(R.id.txtNote)
    TextView txtView;

    DateFormat dateFormat = new SimpleDateFormat("MM-yyyy");

    private ReportService service;
    private NoteService noteService;

    ProgressDialog dialog;

    DatePickerDialog datePickerDialog;

    public ReportActivity(){
        super();

        mengaji = new ArrayList<>();
        hafalan = new ArrayList<>();
        sholat = new ArrayList<>();

        labels = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        datepicker.setKeyListener(null);

        userId = getIntent().getIntExtra(ReportFragment.BUNDLE_USER_ID_KEY,0);
        service = RetrofitFactory.getClient(Constant.BASE_API_URL).create(ReportService.class);
        noteService = RetrofitFactory.getClient(Constant.BASE_API_URL).create(NoteService.class);

        dialog = LoadingFactory.getInstance().build(this,"Getting information from system",Constant.PROGRESS_DIALOG_MESSAGE);

        dialog.show();

        Date date = new Date();
        String formattedDate = dateFormat.format(date);
        datepicker.setText(formattedDate);

        if( userId != 0){
            getData();
        }

        if( UserSharedPreference.getInstance().getUser(this).getRole() == Constant.ROLE_TEACHER_CODE ){
            txtView.setVisibility(View.GONE);

        }else{
            btn.setVisibility(View.GONE);
            editNote.setVisibility(View.GONE);
        }

        Calendar cal = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_DARK, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                if( month < 10 ){
                    datepicker.setText("0"+(month + 1)+"-"+year);
                }else{
                    datepicker.setText((month+1)+"-"+year);
                }
                getData();

            }
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE)){

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);

                int day = this.getContext().getResources().getIdentifier("android:id/day",null,null);
                if( day != 0 ){
                    View dayPicker = findViewById(day);
                    if( dayPicker != null ){
                        dayPicker.setVisibility(View.GONE);
                    }
                }

            }
        };

    }

    private void getData(){
        String[] valueOfDatepicker = datepicker.getText().toString().split("-");

        ReportRequestDTO requestData = new ReportRequestDTO();
        requestData.setUserId(userId);
        requestData.setMonth(Integer.parseInt(valueOfDatepicker[0]));
        requestData.setYear(Integer.parseInt(valueOfDatepicker[1]));

        dialog.show();
        service.report(UserSharedPreference.getInstance()
                .getUser(this).getToken(),requestData).enqueue(new Callback<BaseResponse<ReportResponseDTO>>() {
            @Override
            public void onResponse(Call<BaseResponse<ReportResponseDTO>> call, Response<BaseResponse<ReportResponseDTO>> response) {
                generateData(response.body().getData());
                dialog.hide();
            }

            @Override
            public void onFailure(Call<BaseResponse<ReportResponseDTO>> call, Throwable t) {
                dialog.hide();
            }
        });
    }

    private void generateData(ReportResponseDTO data){

        mengaji.clear();
        sholat.clear();
        hafalan.clear();
        labels.clear();

        List<Report> mengajis = data.getMengaji();
        List<Report> sholats = data.getSholat();
        List<Report> hafalans = data.getMenghafal();

        for (int i = 0; i < mengajis.size(); i++) {
            mengaji.add(new BarEntry((float)(i+1),mengajis.get(i).getTotal()));
            sholat.add(new BarEntry((float)(i+1),sholats.get(i).getTotal()));
            hafalan.add(new BarEntry((float)(i+1),hafalans.get(i).getTotal()));

            labels.add(String.valueOf(i+1));
        }

        BarDataSet mengaji = new BarDataSet(this.mengaji,"Mengaji");
        mengaji.setColor(Color.RED);
        BarDataSet hafalan = new BarDataSet(this.hafalan,"Hafalan");
        hafalan.setColor(Color.GREEN);
        BarDataSet sholat = new BarDataSet(this.sholat,"Sholat");
        sholat.setColor(Color.YELLOW);

        BarData barData = new BarData(mengaji,sholat,hafalan);
        barData.setBarWidth(0.3f);
        barChart.setData(barData);
        barChart.animateY(300);
        barChart.groupBars(0f,0.3f,0.04f);
        barChart.setVisibleXRangeMaximum(7);
        barChart.invalidate(); // refresh
        barChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return labels.get((int)value - 1);
            }
        });

        if(txtView.getVisibility() == View.GONE){
            editNote.setText(data.getNote());
        }else{
            txtView.setText(data.getNote());
        }

    }

    @OnClick(R.id.btnSubmit)
    public void submitNote(View view){

        String[] valueOfDatepicker = datepicker.getText().toString().split("-");

        dialog.show();
        Note note = new Note();
        note.setUserId(userId);
        note.setMonth(Integer.parseInt(valueOfDatepicker[0]));
        note.setYear(Integer.parseInt(valueOfDatepicker[1]));
        note.setNote(editNote.getText().toString());

        noteService.submitData(UserSharedPreference.getInstance().getUser(this).getToken(),note).enqueue(new Callback<BaseResponse<Object>>() {
            @Override
            public void onResponse(Call<BaseResponse<Object>> call, Response<BaseResponse<Object>> response) {
                Toast.makeText(ReportActivity.this,"Note Berhasil perbaharui",Toast.LENGTH_SHORT).show();
                dialog.hide();
            }

            @Override
            public void onFailure(Call<BaseResponse<Object>> call, Throwable t) {
                dialog.hide();
            }
        });
    }

    @OnClick(R.id.datepicker)
    public void clickEvent(View view){
        datePickerDialog.show();
    }

}
