package com.app.monitoringapps.listeners;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.app.monitoringapps.callbacks.UserLocationCallback;

/**
 * TODO: Add a class header comment!
 */

public class UserLocationListener implements LocationListener {

    private UserLocationCallback mCallback;

    public static final int USER_LOCATION_PERMISSION_CHECKED = 100;
    public static final String LOCATION_PERMISSION_DENIED_ERORR_MESSAGE = "Tidak dapat mengambil lokasi saat ini!!!";

    public UserLocationListener(UserLocationCallback callback){
        mCallback = callback;
    }

    @Override
    public void onLocationChanged(Location location) {
        mCallback.getData(location,this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
